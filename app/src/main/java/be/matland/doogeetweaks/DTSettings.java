package be.matland.doogeetweaks;

import android.content.ComponentName;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.view.View;

public class DTSettings extends PreferenceActivity {
    public static final String ACTION_PREFSCHANGED = "doogeetweaks.intent.action.ACTION_PREFSCHANGED";

    private final SharedPreferences.OnSharedPreferenceChangeListener mListener =
            new SharedPreferences.OnSharedPreferenceChangeListener() {
        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
            Intent intent = new Intent();
            intent.setAction(ACTION_PREFSCHANGED);
            sendBroadcast(intent);
            //Log.d("DoogeeTweaks", "Broadcast sent.");
        }
    };

    private final Preference.OnPreferenceChangeListener mHideLauncherListener =
            new Preference.OnPreferenceChangeListener() {
        @Override
        public boolean onPreferenceChange(Preference preference, Object newValue) {
            PackageManager packageManager = getPackageManager();
            int state = (Boolean) newValue ? PackageManager.COMPONENT_ENABLED_STATE_DISABLED : PackageManager.COMPONENT_ENABLED_STATE_ENABLED;
            ComponentName aliasName = new ComponentName(DTSettings.this, "be.matland.doogeetweaks.DTSettingsAlias");
            packageManager.setComponentEnabledSetting(aliasName, state, PackageManager.DONT_KILL_APP);
            return true;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dtsettings);
        if (isModuleEnabled())
            findViewById(R.id.warning).setVisibility(View.GONE);
        PreferenceManager.setDefaultValues(this, R.xml.settings, false);
        //noinspection deprecation
        addPreferencesFromResource(R.xml.settings);
        //noinspection deprecation
        getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(mListener);
        //noinspection deprecation
        findPreference("pref_key_hidelauncher").setOnPreferenceChangeListener(mHideLauncherListener);
    }

    private boolean isModuleEnabled() {
        return false; // This method gets hooked in DoogeeTweaks.java to return true.
    }
}
