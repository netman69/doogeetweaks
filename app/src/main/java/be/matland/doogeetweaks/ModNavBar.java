package be.matland.doogeetweaks;

import android.app.AndroidAppHelper;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_InitPackageResources;
import de.robv.android.xposed.callbacks.XC_LayoutInflated;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

class ModNavBar {
    private View mHideBtn = null, mHideBtnLand = null;
    private Context mContext = null;

    public void handleLoadPackage(final XC_LoadPackage.LoadPackageParam lpparam) {
        if (!lpparam.packageName.equals("com.android.systemui"))
            return;
        //XposedBridge.log("DoogeeTweaks -- SystemUI loaded.");

        final Class<?> CLASS_NAVIGATION_BAR_VIEW = XposedHelpers.findClass("com.android.systemui.statusbar.phone.NavigationBarView", lpparam.classLoader);
        XposedHelpers.findAndHookConstructor(CLASS_NAVIGATION_BAR_VIEW, Context.class, AttributeSet.class, new XC_MethodHook() {
            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                XposedBridge.log("DoogeeTweaks -- NavBar layout inflated.");
                mContext = AndroidAppHelper.currentApplication();
                updatePrefs();
            }
        });
    }

    public void handleInitPackageResources(XC_InitPackageResources.InitPackageResourcesParam resparam) {
        if (!resparam.packageName.equals("com.android.systemui"))
            return;
        XposedBridge.log("DoogeeTweaks -- NavBar resources initialized.");

        // The layout for portrait mode (and context).
        resparam.res.hookLayout("com.android.systemui", "layout", "navigation_layout", new XC_LayoutInflated() {
            @Override
            public void handleLayoutInflated(LayoutInflatedParam liparam) throws Throwable {
                mHideBtn = liparam.view.findViewById(liparam.res.getIdentifier("hide_button_layout", "id", "com.android.systemui"));
                if (mContext == null)
                    mContext = AndroidAppHelper.currentApplication();
                updatePrefs();
            }
        });

        // And the layout for landscape mode.
        resparam.res.hookLayout("com.android.systemui", "layout", "navigation_layout_rot90", new XC_LayoutInflated() {
            @Override
            public void handleLayoutInflated(LayoutInflatedParam liparam) throws Throwable {
                mHideBtnLand = liparam.view.findViewById(liparam.res.getIdentifier("hide_button_layout", "id", "com.android.systemui"));
                if (mContext == null)
                    mContext = AndroidAppHelper.currentApplication();
                updatePrefs();
            }
        });
    }

    public void updatePrefs() {
        if (mContext != null && mHideBtn != null && mHideBtnLand != null) { // This condition is to wait until both are shown.
            if (DoogeeTweaks.pref_navbar_hidehidebtn) {
                mHideBtn.setVisibility(View.INVISIBLE);
                mHideBtnLand.setVisibility(View.INVISIBLE);
            } else {
                mHideBtn.setVisibility(View.VISIBLE);
                mHideBtnLand.setVisibility(View.VISIBLE);
            }
        }
    }
}
