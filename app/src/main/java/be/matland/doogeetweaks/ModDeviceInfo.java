package be.matland.doogeetweaks;

import android.os.Bundle;

import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

class ModDeviceInfo {
    public void handleLoadPackage(final XC_LoadPackage.LoadPackageParam lpparam) {
        if (!lpparam.packageName.equals("com.android.settings"))
            return;

        final Class<?> CLASS_DEVICE_INFO_SETTINGS = XposedHelpers.findClass("com.android.settings.DeviceInfoSettings", lpparam.classLoader);
        XposedHelpers.findAndHookMethod(CLASS_DEVICE_INFO_SETTINGS, "onCreate", Bundle.class, new XC_MethodHook() {
            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                if (!DoogeeTweaks.pref_set_removeadups)
                    return;
                // We use callMethod as not to depend on support library versions matching exactly (DeviceInfoSettings inherits android.support.v14.preference.PreferenceFragment).
                Object prefScreen = XposedHelpers.callMethod(param.thisObject, "getPreferenceScreen");
                Integer prefCount = (Integer) XposedHelpers.callMethod(prefScreen, "getPreferenceCount");
                for (int i = 0; i < prefCount; ++i) {
                    Object preference = XposedHelpers.callMethod(prefScreen, "getPreference", i);
                    String key = (String) XposedHelpers.callMethod(preference, "getKey");
                    if (key.equals("adupsfota_software_update")) {
                        XposedHelpers.callMethod(prefScreen, "removePreference", new Class<?>[]{XposedHelpers.findClass("android.support.v7.preference.Preference", lpparam.classLoader)}, preference);
                        break;
                    }
                    //CharSequence title = (CharSequence) XposedHelpers.callMethod(preference, "getTitle");
                    //XposedBridge.log("DoogeeTweaks -- Settings thing: " + key + " -- " + title + " -- " + preference.getClass().getName());
                }
            }
        });
    }
}
