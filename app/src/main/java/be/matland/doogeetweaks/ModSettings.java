package be.matland.doogeetweaks;

import android.annotation.SuppressLint;
import android.app.AndroidAppHelper;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Base64;

import com.crossbowffs.remotepreferences.RemotePreferences;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage;
import de.robv.android.xposed.callbacks.XCallback;

class ModSettings {
    private boolean isFalseStart = false;

    public void handleLoadPackage(final XC_LoadPackage.LoadPackageParam lpparam) {
        if (!lpparam.packageName.equals("com.android.settings"))
            return;
        XposedBridge.log("DoogeeTweaks -- Settings package loaded.");

        final Class<?> CLASS_TILE_UTILS = XposedHelpers.findClass("com.android.settingslib.drawer.TileUtils", lpparam.classLoader);
        final Class<?> CLASS_SETTINGS_ACTIVITY = XposedHelpers.findClass("com.android.settings.SettingsActivity", lpparam.classLoader);

        XposedHelpers.findAndHookMethod(CLASS_SETTINGS_ACTIVITY, "onCreate", Bundle.class, new XC_MethodHook() {
            @SuppressLint("ApplySharedPref")
            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                Context ctx = AndroidAppHelper.currentApplication();
                RemotePreferences remotePreferences = new RemotePreferences(ctx, "be.matland.doogeetweaks.prefs", "be.matland.doogeetweaks_preferences");
                if (remotePreferences.getBoolean("isFalseStart", false)) {
                    SharedPreferences.Editor edit = remotePreferences.edit();
                    edit.putBoolean("isFalseStart", false);
                    edit.commit();
                    isFalseStart = true;
                    List list = (List) XposedHelpers.callStaticMethod(CLASS_TILE_UTILS, "getCategories", ctx, new HashMap<>());
                    isFalseStart = false;

                    Categories cats = new Categories();
                    int lsize = list.size();
                    for (int i = 0; i < lsize; ++i) {
                        Object cat = list.get(i);
                        CharSequence ctitle = (CharSequence) XposedHelpers.getObjectField(cat, "title");
                        String key = (String) XposedHelpers.getObjectField(cat, "key");
                        Category mcat = new Category(ctitle, key);
                        List tiles = (List) XposedHelpers.getObjectField(cat, "tiles");
                        int count = tiles.size();
                        for (int j = 0; j < count; ++j) {
                            Object tile = tiles.get(j);
                            CharSequence title = (CharSequence) XposedHelpers.getObjectField(tile, "title");
                            Intent intent = (Intent) XposedHelpers.getObjectField(tile, "intent");
                            mcat.add(new Setting(title, intent.getComponent()));
                            //XposedBridge.log("DoogeeTweaks --   Component: " + intent.getComponent());
                        }
                        cats.add(mcat);
                    }
                    edit.putString("categories", cats.serialize());
                    try {
                        edit.commit();
                        // We broadcast the PACKAGE_INSTALL action because if we make our own, android complains it's not protected.
                        Intent bcast = new Intent("android.intent.action.PACKAGE_INSTALL");
                        bcast.setPackage("be.matland.doogeetweaks");
                        bcast.putExtra("isDoogeeTweaks", true);
                        ctx.sendBroadcast(bcast);
                        XposedBridge.log("DoogeeTweaks -- Successfully aquired preference list.");
                    } catch (Exception e) {
                        /* Empty. */
                    }

                    XposedHelpers.callMethod(param.thisObject, "finish");
                }
            }
        });

        // Setting PRIORITY_LOWEST in hopes to cooperate better with other modules that affect settings.
        XposedHelpers.findAndHookMethod(CLASS_TILE_UTILS, "getCategories", Context.class, HashMap.class, new XC_MethodHook(XCallback.PRIORITY_LOWEST) {
            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                // Artificial getting all items.
                if (isFalseStart)
                    return;
                // Get list of packages to ignore.
                ArrayList<ComponentName> cns = new ArrayList<>();
                try {
                    Context ctx = AndroidAppHelper.currentApplication();
                    RemotePreferences remotePreferences = new RemotePreferences(ctx, "be.matland.doogeetweaks.prefs", "be.matland.doogeetweaks_preferences");
                    cns = new ComponentNames(remotePreferences.getString("categories_disabled", ""));
                } catch (Exception e) {
                    XposedBridge.log("DoogeeTweaks -- Failure to read configuration");
                }
                // Do the thing.
                List list = (List) param.getResult();
                for (int i = 0; i < list.size(); ++i) {
                    List tiles = (List) XposedHelpers.getObjectField(list.get(i),"tiles");
                    for (int j = 0; j < tiles.size(); ++j) {
                        Object tile = tiles.get(j);
                        Intent intent = (Intent) XposedHelpers.getObjectField(tile, "intent");
                        //XposedBridge.log("DoogeeTweaks --   Component: " + intent.getComponent());
                        if (cns.contains(intent.getComponent()))
                            tiles.remove(j--);
                    }
                    // Remove empty categories.
                    if (tiles.size() == 0)
                        list.remove(i--);
                }
                param.setResult(list);
            }
        });
    }

    public static class Setting {
        public final CharSequence title;
        public final ComponentName cn;

        Setting(CharSequence title, ComponentName cn) {
            this.title = title;
            this.cn = cn;
        }

        Setting(String s) {
            String[] ss = s.split("#");
            title = new String(Base64.decode(ss[0], Base64.DEFAULT), StandardCharsets.UTF_8);
            cn = new ComponentName(ss[1], ss[2]);
        }

        public String serialize() {
            String ret = "";
            ret += Base64.encodeToString(title.toString().getBytes(StandardCharsets.UTF_8), Base64.DEFAULT | Base64.NO_WRAP);
            ret += "#";
            ret += cn.getPackageName();
            ret += "#";
            ret += cn.getClassName();
            return ret;
        }
    }

    public static class Category extends ArrayList<Setting> {
        public final CharSequence title;
        public final String key;

        Category(CharSequence title, String key) {
            super();
            this.title = title;
            this.key = key;
        }

        Category(String s) {
            super();
            String[] ss = s.split(":");
            title = new String(Base64.decode(ss[0], Base64.DEFAULT), StandardCharsets.UTF_8);
            key = new String(Base64.decode(ss[1], Base64.DEFAULT), StandardCharsets.UTF_8);
            String[] sss = ss[2].split("\\|");
            for (String it : sss)
                if (it.length() > 0)
                    add(new Setting(it));
        }

        public String serialize() {
            StringBuilder ret = new StringBuilder();
            ret.append(Base64.encodeToString(title.toString().getBytes(StandardCharsets.UTF_8), Base64.DEFAULT | Base64.NO_WRAP));
            ret.append(":");
            ret.append(Base64.encodeToString(key.getBytes(StandardCharsets.UTF_8), Base64.DEFAULT | Base64.NO_WRAP));
            ret.append(":");
            int count = size();
            for (int i = 0; i < count; ++i)
                ret.append(get(i).serialize()).append("|");
            return ret.toString();
        }
    }

    public static class Categories extends ArrayList<Category> {
        Categories() {
            super();
        }

        Categories(String s) {
            super();
            String[] ss = s.split("@");
            for (String it : ss)
                if (it.length() > 0)
                    add(new Category(it));
        }

        public String serialize() {
            StringBuilder ret = new StringBuilder();
            int count = size();
            for (int i = 0; i < count; ++i)
                ret.append(get(i).serialize()).append("@");
            return ret.toString();
        }
    }

    public static class ComponentNames extends ArrayList<ComponentName> {
        ComponentNames() {
            super();
        }

        ComponentNames(String str) {
            super();
            if (str == null)
                return;
            String[] disabled = str.split("\\|");
            for (String s : disabled) {
                if (s.length() <= 0)
                    continue;
                String[] ss = s.split("#");
                add(new ComponentName(ss[0], ss[1]));
            }
        }

        public String serialize() {
            StringBuilder set = new StringBuilder();
            int count = size();
            for (int i = 0; i < count; ++i) {
                ComponentName cn = get(i);
                set.append(cn.getPackageName() + "#" + cn.getClassName() + "|");
            }
            return set.toString();
        }
    }
}
