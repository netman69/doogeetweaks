package be.matland.doogeetweaks;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;

public class DTSSetdisable extends PreferenceActivity {
    private PreferenceManager prefman;
    private PreferenceScreen screen;
    private SharedPreferences sprefs;
    private ModSettings.ComponentNames cns;

    private final BroadcastReceiver catReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle extra = intent.getExtras();
            if (extra != null && extra.getBoolean("isDoogeeTweaks", false))
                update();
        }
    };

    private final Preference.OnPreferenceChangeListener changeListener = new Preference.OnPreferenceChangeListener() {
        @Override
        public boolean onPreferenceChange(Preference preference, Object o) {
            if (!preference.getClass().equals(MyCheckBoxPreference.class))
                return false;
            MyCheckBoxPreference pref = (MyCheckBoxPreference) preference;
            if ((Boolean) o && !cns.contains(pref.componentName))
                cns.add(pref.componentName);
            else if (cns.contains(pref.componentName))
                cns.remove(pref.componentName);

            SharedPreferences.Editor edit = sprefs.edit();
            edit.putString("categories_disabled", cns.serialize());
            edit.apply();
            return true;
        }
    };

    @SuppressLint("ApplySharedPref")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //noinspection deprecation
        prefman = getPreferenceManager();
        screen = prefman.createPreferenceScreen(this);
        sprefs = prefman.getSharedPreferences();

        registerReceiver(catReceiver, new IntentFilter("android.intent.action.PACKAGE_INSTALL"));
        SharedPreferences.Editor edit = sprefs.edit();
        edit.putBoolean("isFalseStart", true);
        edit.commit();
        startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);

        update();
        screen.setTitle(R.string.pref_set_disable);
        //noinspection deprecation
        setPreferenceScreen(screen);
    }

    private void update() {
        String str = sprefs.getString("categories", null);
        ModSettings.ComponentNames ncns = new ModSettings.ComponentNames(sprefs.getString("categories_disabled", null));
        if (str == null)
            return;
        cns = new ModSettings.ComponentNames(); // We rebuild it in loop later, so that lost items are dropped.
        @SuppressWarnings("MismatchedQueryAndUpdateOfCollection")
        ModSettings.Categories cats = new ModSettings.Categories(str);
        screen.removeAll();
        for (int i = 0; i < cats.size(); ++i) {
            ModSettings.Category c = cats.get(i);
            PreferenceCategory cat = new PreferenceCategory(this);
            cat.setTitle(c.title);
            screen.addPreference(cat);
            for (int j = 0; j < c.size(); ++j) {
                final ModSettings.Setting set = c.get(j);
                if (ncns.contains(set.cn))
                    cns.add(set.cn);
                MyCheckBoxPreference pref = new MyCheckBoxPreference();
                pref.setTitle(set.title);
                pref.componentName = set.cn;
                if (cns.contains(set.cn))
                    pref.setChecked(true);
                pref.setOnPreferenceChangeListener(changeListener);
                cat.addPreference(pref);
            }
        }
    }

    class MyCheckBoxPreference extends CheckBoxPreference {
        public ComponentName componentName;

        MyCheckBoxPreference() {
            super(DTSSetdisable.this);
        }
    }
}
