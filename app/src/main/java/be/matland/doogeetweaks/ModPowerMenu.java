package be.matland.doogeetweaks;

import java.util.ArrayList;

import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

class ModPowerMenu {
    public void handleLoadPackage(final XC_LoadPackage.LoadPackageParam lpparam) {
        if (!lpparam.packageName.equals("android"))
            return;

        final Class<?> CLASS_GLOBAL_ACTIONS_DIALOG = XposedHelpers.findClass("com.android.server.policy.GlobalActions", lpparam.classLoader);
        XposedHelpers.findAndHookMethod(CLASS_GLOBAL_ACTIONS_DIALOG, "createDialog", new XC_MethodHook() {
            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                if (!DoogeeTweaks.pref_pwr_removetip)
                    return;
                ArrayList items = (ArrayList) XposedHelpers.getObjectField(param.thisObject,"mItems");
                for (int i = items.size() - 1; i >= 0; --i) {
                    if (items.get(i).getClass().getName().equals("com.android.server.policy.GlobalActions$TipsAction")) {
                        //XposedBridge.log("DoogeeTweaks -- Removing action " + i + " class " + items.get(i).getClass().getName());
                        items.remove(i);
                        break;
                    }
                }
            }
        });
    }
}
