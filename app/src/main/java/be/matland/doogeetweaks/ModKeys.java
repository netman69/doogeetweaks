package be.matland.doogeetweaks;

import android.view.KeyEvent;

import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

class ModKeys {
    public void handleLoadPackage(final XC_LoadPackage.LoadPackageParam lpparam) {
        if (!lpparam.packageName.equals("android"))
            return;

        final Class<?> CLASS_PHONE_WINDOW_MANAGER = XposedHelpers.findClass("com.android.server.policy.PhoneWindowManager", lpparam.classLoader);
        //XposedBridge.log("DoogeeTweaks -- PhoneWindowManager loaded.");

        XposedHelpers.findAndHookMethod(CLASS_PHONE_WINDOW_MANAGER, "interceptKeyBeforeQueueing", KeyEvent.class, int.class, new XC_MethodHook() {
            @Override
            protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                if (DoogeeTweaks.pref_btn_cam_fix && DoogeeTweaks.pref_btn_cam_skipdefault && ((KeyEvent) param.args[0]).getKeyCode() == KeyEvent.KEYCODE_CAMERA)
                    param.setResult(1);
                if (DoogeeTweaks.pref_btn_ptt_skipdefault && ((KeyEvent) param.args[0]).getKeyCode() == 280)
                    param.setResult(1);
                if (DoogeeTweaks.pref_btn_sos_skipdefault && ((KeyEvent) param.args[0]).getKeyCode() == 281)
                    param.setResult(1);
            }

            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                if (DoogeeTweaks.pref_btn_cam_fix && !DoogeeTweaks.pref_btn_cam_skipdefault && ((KeyEvent) param.args[0]).getKeyCode() == KeyEvent.KEYCODE_CAMERA)
                    param.setResult(1);
            }
        });
    }
}
