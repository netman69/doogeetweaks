package be.matland.doogeetweaks;

import android.app.AndroidAppHelper;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.AttributeSet;

import com.crossbowffs.remotepreferences.RemotePreferenceAccessException;
import com.crossbowffs.remotepreferences.RemotePreferences;

import de.robv.android.xposed.IXposedHookInitPackageResources;
import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_InitPackageResources.InitPackageResourcesParam;
import de.robv.android.xposed.callbacks.XC_LoadPackage.LoadPackageParam;

public class DoogeeTweaks implements IXposedHookInitPackageResources, IXposedHookLoadPackage {
    private final ModNavBar modNavBar = new ModNavBar();
    private final ModKeys modKeys = new ModKeys();
    private final ModPowerMenu modPowerMenu = new ModPowerMenu();
    private final ModSettings modSettings = new ModSettings();
    private final ModDeviceInfo modDeviceInfo = new ModDeviceInfo();

    private RemotePreferences mRemotePreferences;
    private final BroadcastReceiver mBroadcastReceiver =  new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action == null)
                return;
            if (action.equals(DTSettings.ACTION_PREFSCHANGED))
                updatePrefs();
            if (action.equals(Intent.ACTION_LOCKED_BOOT_COMPLETED))
                updatePrefs();
            //XposedBridge.log("DoogeeTweaks -- Handled broadcast.");
        }
    };

    // Settings.
    public static boolean pref_navbar_hidehidebtn = true; // I set so the hide button is hidden until setting known.
    public static boolean pref_btn_cam_fix = true;
    public static boolean pref_btn_cam_skipdefault = true;
    public static boolean pref_btn_ptt_skipdefault = true;
    public static boolean pref_btn_sos_skipdefault = true;
    public static boolean pref_pwr_removetip = true;
    public static boolean pref_set_removeadups = false;

    private void updatePrefs() {
        // The default values here should match defaults in settings.xml.
        try {
            pref_navbar_hidehidebtn = mRemotePreferences.getBoolean("pref_key_navbar_hidehidebutton", false);
            pref_btn_cam_fix = mRemotePreferences.getBoolean("pref_key_btn_cam_fix", true);
            pref_btn_cam_skipdefault = mRemotePreferences.getBoolean("pref_key_btn_cam_skipdefault", false);
            pref_btn_ptt_skipdefault = mRemotePreferences.getBoolean("pref_key_btn_ptt_skipdefault", false);
            pref_btn_sos_skipdefault = mRemotePreferences.getBoolean("pref_key_btn_sos_skipdefault", false);
            pref_pwr_removetip = mRemotePreferences.getBoolean("pref_key_pwr_removetip", false);
            pref_set_removeadups = mRemotePreferences.getBoolean("pref_key_set_removeadups", false);
        } catch (RemotePreferenceAccessException e) {
            /* Empty. */
        }
        //XposedBridge.log("DoogeeTweaks -- Setting is " + pref_navbar_hidehidebtn + ".");
        modNavBar.updatePrefs();
    }

    @Override
    public void handleLoadPackage(final LoadPackageParam lpparam) throws Throwable {
        modNavBar.handleLoadPackage(lpparam);
        modKeys.handleLoadPackage(lpparam);
        modPowerMenu.handleLoadPackage(lpparam);
        modSettings.handleLoadPackage(lpparam);
        modDeviceInfo.handleLoadPackage(lpparam);

        if (lpparam.packageName.equals("com.android.systemui")) {
            final Class<?> CLASS_NAVIGATION_BAR_VIEW = XposedHelpers.findClass("com.android.systemui.statusbar.phone.NavigationBarView", lpparam.classLoader);
            XposedHelpers.findAndHookConstructor(CLASS_NAVIGATION_BAR_VIEW, Context.class, AttributeSet.class, new XC_MethodHook() {
                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    XposedBridge.log("DoogeeTweaks -- Register receiver for NavigationBarView.");
                    registerReceiver();
                }
            });
        }

        if (lpparam.packageName.equals("com.android.settings")) {
            final Class<?> CLASS_SETTINGS_ACTIVITY = XposedHelpers.findClass("com.android.settings.SettingsActivity", lpparam.classLoader);
            XposedHelpers.findAndHookMethod(CLASS_SETTINGS_ACTIVITY, "onCreate", Bundle.class, new XC_MethodHook() {
                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    XposedBridge.log("DoogeeTweaks -- Register receiver for SettingsActivity.");
                    registerReceiver();
                }
            });
        }

        if (lpparam.packageName.equals("android")) {
            final Class<?> CLASS_PHONE_WINDOW_MANAGER = XposedHelpers.findClass("com.android.server.policy.PhoneWindowManager", lpparam.classLoader);
            XposedBridge.log("DoogeeTweaks -- PhoneWindowManager loaded.");
            XposedHelpers.findAndHookMethod(CLASS_PHONE_WINDOW_MANAGER, "init", Context.class, "android.view.IWindowManager", "android.view.WindowManagerPolicy.WindowManagerFuncs", new XC_MethodHook() {
                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    XposedBridge.log("DoogeeTweaks -- Register receiver for PhoneWindowManager.");
                    registerReceiver();
                }
            });
        }

        if (lpparam.packageName.equals("be.matland.doogeetweaks")) {
            final Class<?> CLASS_DTSETTINGS = XposedHelpers.findClass("be.matland.doogeetweaks.DTSettings", lpparam.classLoader);
            XposedHelpers.findAndHookMethod(CLASS_DTSETTINGS, "isModuleEnabled", new XC_MethodHook() {
                @Override
                protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                    param.setResult(true);
                }
            });
        }
    }

    @Override
    public void handleInitPackageResources(InitPackageResourcesParam resparam) throws Throwable {
        modNavBar.handleInitPackageResources(resparam);
    }

    private void registerReceiver() {
        Context ctx = AndroidAppHelper.currentApplication();
        mRemotePreferences = new RemotePreferences(ctx, "be.matland.doogeetweaks.prefs", "be.matland.doogeetweaks_preferences", true);
        IntentFilter filter = new IntentFilter();
        filter.addAction(DTSettings.ACTION_PREFSCHANGED);
        filter.addAction(Intent.ACTION_LOCKED_BOOT_COMPLETED);
        ctx.registerReceiver(mBroadcastReceiver, filter);
        updatePrefs();
    }
}
