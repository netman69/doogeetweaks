package be.matland.doogeetweaks;

import com.crossbowffs.remotepreferences.RemotePreferenceProvider;

public class DTSettingsProvider extends RemotePreferenceProvider {
    public DTSettingsProvider() {
        super("be.matland.doogeetweaks.prefs", new String[] { "be.matland.doogeetweaks_preferences" });
    }
}
