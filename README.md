******************
** DoogeeTweaks **
******************

An attempt to fix some quirks of the Doogee S60 firmware with an Xposed module.

What it can do:
	- Hide the little down arrow on the navigation bar that hides said bar when pressed.  
	- Make the camera button work with third party camera apps.  
	- Disable the default behaviour of PTT, SOS and camera buttons.  
	- Remove that ugly explanation about how to force rebooting in the power menu.  
	- Take adups menu item out of about section in settings.  
	- Hide items for disabled/uninstalled packages from the settings app.  
